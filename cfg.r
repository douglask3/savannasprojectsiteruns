################################################################################
## package and library paths                                                  ##
################################################################################
library("gitProjectExtras")
setupProjectStructure()
sourceAllLibs()
sourceAllLibs('libs/lpxcheckoutlibs/')

paths    <- c('../rasterExtraFuns/', '../extraLibs/', 'variableConstruction')

packages <- c('raster', 'gitProjectExtras', 'outliers', 'sp', 'ncdf4')


lapply(paths, sourceAllLibs)
lapply(packages, install_and_source_library)


LPXInsDir <- '/home/doug/Documents2/climInputs/climData/historic'


################################################################################
## Variable info                                                              ##
################################################################################
setupProjectStructure()

soilDepth = seq(2, 6, 0.1)

VaribaleConstructInfo <- rbind(
    tas     = list('csv', 'Ta_Con'          , meanE        , list('+', 273.15)),
    tasmax  = list('csv', 'Ta_Con'          , mean.dailyMax, list('+', 273.15)),
    tasmin  = list('csv', 'Ta_Con'          , mean.dailyMin, list('+', 273.15)),
    pr      = list('csv', 'Precip_Con'      , sumE         , list('/', 86400 )),
    wetdays = list('csv', 'Precip_Con'      , noZero       , NULL             ),
    swrad   = list('csv', 'Fsd_Con'         , meanE        , list('/', 1000)  ),
    lwrad   = list('csv', 'Flu_Con'         , meanE        , list('/', 1000)  ),
    uvas    = list('csv', 'Ws_CSAT_Con'     , meanE        , NULL             ),
    a_nd    = list('ncd', 'a_nd_fill.nc'    , 'single'     , NULL             ),
    crop    = list('ncd', 'cropland_18512006.nc', 'annual' , NULL             ),
    mask    = list('val', 1                 ,'single'      , NULL             ),
    soiltype= list('ncd', '../soildata_0k.nc','single'     , NULL             ),
    soildepth=list('val', soilDepth         , 'siteVar'    , NULL             ),
    crop    = list('ncd', 'cropland_18512006.nc', 'annual' , NULL             ),
    lightn  = list('ncd','lightn_climatology_otd_mlnha.nc','clim', NULL       ),
    pas     = list('ncd', 'pas_fill2.nc'    , 'annual'     , NULL             ),
    popdens = list('ncd', 'popdens_fill2.nc', 'annual'     , NULL             ),
    co2     = list('csz', 'co2_mlo_monthly-noaa-esrl.csv' , 'annual'     , NULL             ),
    c13     = list('csz', 'co2_mlo_monthly-noaa-esrl.csv' , 'annaul'     , NULL             ),
    c14     = list('csz', 'co2_mlo_monthly-noaa-esrl.csv' , 'annual'     , NULL             ))

monthLength = c(31, 28, 31, 20, 31, 30, 31, 31, 30, 31, 30, 31)

################################################################################
## Site info                                                                  ##
################################################################################
siteLocInfo = rbind(
    AdelaideRiver   = c('131.07.04.08E', '13.04.36.84S'),
    AliceSprings    = c('133.43.10.49E', '22.22.44.77S'),
    Calperum        = c('140.35.26.10E', '34.00.16.30S'),
    DalyPasture     = c('131.19.05.00E', '14.03.48.00S'),
    DalyUncleared   = c('130.23.17.16E', '14.09.33.12S'),
    DryRiver        = c('132.22.14.04E', '15.15.31.62S'),
    FoggDam         = c('131.18.25.86E', '12.32.42.79S'),
    GWW             = c('121.02.48.33E', '29.50.19.89S'),
    HowardSprings   = c('131.09.09.00E', '12.29.39.12S'),
    RDMF            = c('132.23.32.52E', '14.30.53.18S'),
    Riggs           = c('145.34.33.56E', '36.38.59.59S'),
    SturtPlains     = c('133.21.01.14E', '17.09.02.76S'),
    Tumbarumba      = c('148.09.06.00E', '35.39.23.80S'),
    Wallaby         = c('145.11.14.10E', '37.25.34.40S'),
    Whroo           = c('145.01.34.37E', '36.40.22.98S'),
    Wombat          = c('144.06.04.88E', '37.25.35.91S'),
    Yanco_JAXA      = c('146.17.27.62E', '34.59.21.59S'))

siteLocInfo[] = sapply(siteLocInfo, dms2dd)
class(siteLocInfo) = 'numeric'
