apply.namePass <- function(X, MARGIN, FUN, ...) {

    if (MARGIN == 1) names = rownames(X)
    if (MARGIN == 2) names = colnames(X)

    X = unlist(apply(X, MARGIN, list), recursive=FALSE)
    
    dat = mapply(FUN, X, names, ...)
    names(dat) = names
    return(dat)
}
