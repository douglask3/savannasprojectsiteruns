dms2dd <- function(d, m = NULL, s = NULL, dir = NULL) {
    if (is.null(m)) {

        dir     = substr(d,nchar(d), nchar(d))
        if(!tryTest(as.numeric(dir)) && !is.na(as.numeric(dir))) dir = NULL

        d = strsplit(d, '.', TRUE)[[1]]
        m = d[2]

        if (length(d)==4) s = paste(d[3], d[4], sep = '.')
            else s = d[3]
        if (!is.null(dir)) s = substr(s, 1, nchar(s) - 1)

        d = d[1]
    }
    c(d, m, s) := lapply(c(d, m, s), as.numeric)

    d = d + m/60 + s / 3600

    if (!is.null(dir) && (dir == 'W' || dir == 'S'))
        d = - d
    return(d)
}
