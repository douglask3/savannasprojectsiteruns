lpj.cfgStandard <- function(filenames, siteName, climPath = '',
                            outPath = outputs_dir,
                            outputVars = c('mcflux_fire', 'mnpp_grid',
                                           'mrh_grid', 'mapar',
                                           'cflux_fast_atmos',
                                           'cflux_slow_atmos')) {
    filenames = unlist(filenames)
    lpj.cfg(climFilePath = climPath,
                filenames['tas'],  filenames['tasmin'],  filenames['tasmax'],
                filenames['pr'], filenames['wetdays'], filenames['uvas'],
                NULL, sunIsCloud = TRUE, filenames['lwrad'], filenames['swrad'],
            LgtnFilePath = climPath, filenames['lightn'],
            maskFilePath = climPath, filenames['mask'], maskVar = "mask",
                maskType = "BOOLEAN",
            soilFilePath = climPath,
                filenames['soiltype'], soilVar = "soiltype",
                filenames['soildepth'], soildepthVar = "soildepth",
            humnFilePath = climPath, filenames['popdens'], filenames['a_nd'],
            agriFilePath = climPath, filenames['crop'], filenames['pas'],
            CrbnFilePath = climPath,
                filenames['co2'], CO2Var = 'co2',
                filenames['c13'], C13Var = 'c13',
                filenames['c14'], filenames['c14'],
                    filenames['c14'], C14Var = 'c14',
            nyrsSpinup = 100, nyrsRampup = 0, nyrsRun = 1,
            startYearsOutput = 1, frequancyRunup = 1,
            spinFilePath = outPath, spinupFile = '',
            rampupFile = '',
            outputFilePath = outPath, siteName,
            outputVars = outputVars,
            includeProjectVersionNumber = TRUE)


}
