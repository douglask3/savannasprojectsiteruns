makeFilenameRasterNcOut <- function(siteName, varname) {
    spinUp = c("spinUp", "")
    fname <- function(i) {
        filename = paste(outputs_dir, siteName, spinUp, i, gitVersionNumber(),
                         '.nc', sep ="-")
        names(filename) = rep(i, 2)
        return(filename)
    }
    filename = lapply(varname, fname)
    return(filename)
}
