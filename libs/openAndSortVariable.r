openAndSortVariable <- function(file, siteName, timeVars, varnames) {
    varType =  unlist(VaribaleConstructInfo[varnames, 1])

    dataVal = openAndSortValVars(varnames[varType == 'val'], siteName)

    dataCsv = openAndSortCsvVars(siteName, file, timeVars,
                                 varnames[varType == 'csv'])

    dataNcd = openAndSortNcdVars(varnames[varType == 'ncd'], file, siteName,
                                 timeVars)

    dataNcz = openAndSortCszVars(siteName, file, timeVars,
                                 varnames[varType == 'csz'])
    return(list(dataCsv, dataNcd, dataVal, dataNcz))
}

################################################################################
## Open cvs based variables                                                   ##
################################################################################
openAndSortCszVars <- function(siteName, file, timeVars, varnames) {
    varFile = VaribaleConstructInfo[varnames, 2]
    varFuns = VaribaleConstructInfo[varnames, 3]
    varScle = VaribaleConstructInfo[varnames, 4]
    fnames  = makeFilenameRasterNcOut(siteName, varnames)

    returnIfExists(fnames)(fnames)

    time    = openVariable.csv(file, tail(timeVars,1))
    time    = unique(time)

    readCsvAndFindYr <- function(i) {
        dat = read.csv(paste(data_dir, i, sep = '/'))
        ind = apply(sapply(t(time), function(i) dat[,1] == i), 1, sum)
        return(dat[ind, 2])
    }
    dat = lapply(varFile, readCsvAndFindYr)

    mapply(write1Dncdf, dat, siteName, names(dat))
}
################################################################################
## Open cvs based variables                                                   ##
################################################################################
returnIfExists <- function(filenames) {
    if (all(file.exists(unlist(filenames)))) return(return)
        else return(function(i) return(NULL))
}

openAndSortCsvVars <- function(siteName, file, timeVars, varnames) {
    filenames = makeFilenameRasterNcOut(siteName, varnames)

    #if (all(file.exists(unlist(filenames)))) return(filenames)
    returnIfExists(filenames)(filenames)

    varVars =  VaribaleConstructInfo[varnames, 2]
    varFuns =  VaribaleConstructInfo[varnames, 3]
    varScle =  VaribaleConstructInfo[varnames, 4]

    tempFle = constructTempFile(siteName, varnames)

    if(file.exists(tempFle)) data = read.csv(tempFle, row.names = 1)
    else {
        rawVars = unlist(c(timeVars, varVars))
        data    = openVariable.csv(file, rawVars)

        data    = mapply(funVariable, varVars, varFuns, varScle,
                         MoreArgs = list(data, timeVars))

        write.csv(data, tempFle)
    }
    nExtra    = 12 - baseN(dim(data)[1], 12)

    data      = apply(data, 2, function(i) c(i, rep(-999, nExtra)))
    filenames = apply.namePass(data, 2, write.netcdf, siteName, yrL = 12)
    return(filenames)
}

constructTempFile <- function(siteName, varnames)
    paste(c('temp/', siteName, varnames, '.csv'), collapse = '')


funVariable <- function(var, fun, scle, datas, timeVars) {
    data  = datas[, var]
    times = datas[timeVars]

    data  = fun(data, times)

    if(!is.null(scle)) data  = match.fun(scle[[1]])(data, scle[[2]])

    return(data)
}

################################################################################
## Open ncdf based variables                                                  ##
################################################################################
openAndSortNcdVars <- function(varnames, file, siteName, timeVars) {
    filenames = makeFilenameRasterNcOut(siteName, varnames)

    returnIfExists(filenames)(filenames)

    siteLoc     = siteLocInfo[siteName, ]
    varFile     = VaribaleConstructInfo[varnames, 2]
    varLngth    = VaribaleConstructInfo[varnames, 3]

    varFile     = paste(LPXInsDir, varFile, sep ='/')


    data        = mapply(openVariable.ncd, varFile, varnames,
                       MoreArgs = list(siteLoc))
    names(data) = varnames

    years       = unique(read.csv(file)[timeVars[5]])
    data[varLngth == 'annual'] = lapply(data[varLngth == 'annual'], findAnnuals, years)

    yrL = rep(1, length(varLngth))
    yrL[varLngth == 'single'] = NaN
    yrL[varLngth == 'clim'] = NaN

    filenames   = mapply(write.netcdf, data, names(data), siteName, yrL = yrL)
    names(filenames) = varnames
    return(filenames)
}

findAnnuals <- function(dat, timeVars)
    rep(tail(t(dat), 1), dim(timeVars)[1])

################################################################################
## make value based variables                                                 ##
################################################################################
openAndSortValVars <- function(varnames, siteName) {
    varVals = VaribaleConstructInfo[varnames, 2]
    varType = VaribaleConstructInfo[varnames, 3]

    makeData <- function(type, val) {
        if (type == 'single') return(val)
        if (type == 'siteVar') return(rep(val, length.out = length(soilDepth)))
    }

    data    = mapply(makeData, varType, varVals, SIMPLIFY = FALSE)
    fnames  = mapply(write.netcdf, data, varnames, siteName, varType,
                     MoreArgs = list(yrL = NULL), SIMPLIFY = FALSE)
    names(fnames) = NULL
    return(fnames)
}

################################################################################
## writing inputs                                                             ##
################################################################################
write.netcdf <- function(dat, varname, siteName, yrL = 12, ...) {
    filename = makeFilenameRasterNcOut(siteName, varname)

    if (length(filename) == 1) filename = filename[[1]]
    write.netcdf.gubbins(dat  , filename[[2]], varname, ...)

    if (is.null(yrL) || is.na(yrL)) {
        write.netcdf.gubbins(dat, filename[[1]], varname, ...)
    } else {
        if (length(dat) == 1) spDat = rep(dat, 50)
        else spDat = rep(dat[1:yrL], 50)

        write.netcdf.gubbins(spDat, filename[[1]], varname, ...)
    }
    return(filename)
}

write.netcdf.gubbins <- function(dat, fname, varname,  varType = "") {

    dati = c()
    if (class(dat) == "matrix") dat = as.numeric(dat)
    if (varType == "siteVar") dati = cbind(1:length(soilDepth), 1, dat)
    else for (i in 1:length(soilDepth))
            dati = rbind(dati, cbind(i, 1, t(dat)))

    ### These 3 columns are prioxies for (1) rooting depth (done here); (2) HR
    ### and (3) c3/c4 experiments that have not been defined yet.
    setxn <- function(n) {dati[,2] = n; dati}
    dat = rbind(setxn(1), setxn(2), setxn(3))

    dat = rasterFromXYZ(dat,
                 crs = "+proj=lcc +lat_1=48 +lat_2=33 +lon_0=-100 +ellps=WGS84")

    # look at draught experiment for lpx format in write Raster
    writeRaster(dat, fname, varname = varname, overwrite = TRUE,
                xname = 'lon', yname = 'lat', zname = 'time',zunit = 'month')
    return(fname)
}
