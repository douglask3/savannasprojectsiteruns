openVariable.csv <- function(file, cnames)
    read.csv(file)[cnames]

openVariable.ncd <- function(file, varname, siteLoc) {
    dat = brick(file, varname = varname)
    return(dat[cellFromXY(dat, siteLoc)])
}
