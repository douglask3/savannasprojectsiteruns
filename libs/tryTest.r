tryTest <- function(...) 
    class(suppressWarnings(try(..., silent = TRUE))) != "try-error"
