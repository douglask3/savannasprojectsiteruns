write1Dncdf <- function (dat, siteName, varname) {
    fnames =  makeFilenameRasterNcOut(siteName, varname)
    if (class(fnames) == 'list') fnames = unlist(fnames)
    write1Dncdf.gubbins(rep(dat[1],50), fnames[1], varname)
    write1Dncdf.gubbins(dat           , fnames[2], varname)
    return(fnames)
}

write1Dncdf.gubbins <- function(dat, fname, varname) {
    nt    = length(dat)
    dimT  = ncdim_def( "time", "month", 1:length(dat))

    mv    = -999
    var1d = ncvar_def(varname, "units", dimT, mv )

    nc    = nc_create(fname, var1d )

    ncvar_put(nc, var1d, dat)  # no start or count: write all values

    nc_close(nc)
    return(fname)
}
