source('cfg.r')

timeVars  = c( 'Minute', 'Hour', 'Day', 'Month', 'Year')
Variables = rownames(VaribaleConstructInfo)

files     = list.files('data/', full.names = TRUE)

strsplit1 = function(i, x) strsplit(i, x)[[1]]
siteFiles = files[grepl("Advanced",files)]
siteNames = sapply(siteFiles, strsplit1, 'Advanced_processed_data_')[2, ]
siteNames = sapply(siteNames, strsplit1, '_v11b.csv')

filenames = mapply(openAndSortVariable, siteFiles, siteNames,
                   MoreArgs = list(timeVars, Variables), SIMPLIFY = FALSE)

checkoutLPX('roots') #5d60344  283eb91 85222cb

runModel <- function(filenames, ...) {
    lpj.cfgStandard(filenames, ...)
    runLPX(step1a = TRUE)
    lpj.cfgStandard(filenames, ...)
    runLPX(step2  = TRUE)
}

mapply(runModel, filenames, siteNames)
