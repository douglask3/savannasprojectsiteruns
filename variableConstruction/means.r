mean.dailyMax <- function(dat, times, fun = max) {
    cnames = c('Day', 'Month', 'Year')
    dat    = splitFun(dat, times, cnames, fun)
    dat    = dat[!is.na(dat)]
    times  = unique(times[, cnames])

    dat    = splitFun(dat, times, c('Month', 'Year'), mean)
    return(dat)
}

mean.dailyMin <- function(...) mean.dailyMax(fun = min, ...)

meanE <- function(dat, times)
    splitFun(dat, times, c('Month', 'Year'), mean)

sumE <- function(...) {
    dat = meanE(...)
    return(multByMonth(dat))
}


splitFun <- function(dat, times, colnames, fun = mean) {
    times = times[, colnames]
    dat   = suppressWarnings(split (dat, times))
    dat   = dat[sapply(dat,length)!=0]
    dat   = sapply(dat, fun )

    return(dat)
}
