noZero <- function(dat, times) {

    dat = dat != 0

    dat = splitFun(dat, times, c('Month', 'Year'), mean)

    return(multByMonth(dat))
}

multByMonth <- function(dat) {
    refM = sapply(names(dat),
                  function(i) strsplit(i,'.', TRUE)[[1]][1])

    refM = as.numeric(refM)
    return(dat * monthLength[refM])
}
